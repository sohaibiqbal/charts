
// Defind context object

/**
 * ---------------------------------------------------------------
 *  Initialize tree, listing tools
 * ---------------------------------------------------------------
 */
function initToolsTree() {
    $('#jstree_tools').jstree({
     
        'core': {
            'data': [
               
                  { "id": "gcharts", "parent": "#", "text": "Google Charts" },
                  { "id": "pie-chart", "parent": "gcharts", "text": "Pie Chart" },
                  { "id": "column-chart", "parent": "gcharts", "text": "Column Chart" },
                  { "id": "scatter-chart", "parent": "gcharts", "text": "Scatter Chart" },
                  { "id": "trendlines", "parent": "gcharts", "text": "Trendlines" }, // to add a new chart, add a new line after this one
            ]
        }
    });

    /**
     *  listen for event
     */
    $('#jstree_tools').on('changed.jstree', function (e, data) {
   

        var i, j, n = [];
        var ids = [];

        for (i = 0, j = data.selected.length; i < j; i++) {
            // selected node text
            n.push(data.instance.get_node(data.selected[i]).text);
            // selected node ids	
            ids.push(data.instance.get_node(data.selected[i]).id);
            break;
        }
        // get parent of selected node       
        var parent = data.instance.get_parent(ids[0]);
        var id = data.node.id;
  
  
            localStorage.removeItem("pageID");
            localStorage.setItem("pageID", id);
    
      

        loadMainContent(n, id);


    })
     // create the instance
     .jstree();


    if (checkBrowserSupport()) {
   
        var prevID = localStorage.getItem("pageID");
        if (prevID !== null)
            $("#jstree_tools").jstree("select_node", prevID);
           }
}

/**
 *  Load dynamically the pages
 */
function loadMainContent(selected, id) {
    var rootdir = window.location.origin;
 

    $(".download-buttons").css({ "visibility": 'hidden' });
    $("#chart_div").html("");
    $("#row-chart-title").attr('class', 'row show');
    $("#generateChart").show();
    $("#btn-clear-storage").attr('class', 'btn btn-primary show');
    $("#div-size-panel").attr('class', 'btn-group show');
    switch (id) {
        case 'pie-chart':
            $("#main_content").load("./components/charts/pie-chart.html");
            break;
        case 'column-chart':
            $("#main_content").load("./components/charts/column-chart.html");
            break;
        case 'scatter-chart':
            $("#main_content").load("./components/charts/scatter-chart.html");
            break;
        case 'trendlines':
            $("#main_content").load("./components/charts/trendlines.html");
            break;
        default:
            alert('gcharts not matched');
            break;
    }

  




   

   
}






