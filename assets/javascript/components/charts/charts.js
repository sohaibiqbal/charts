/* 
*Global variables
*/
var imageURI = null;

/* 
*Method to draw Pie Chart
*/
drawPieChart = function (CatName, CatPoints,title,width,height) {


    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Catagories');
    data.addColumn('number', 'Percent');


    data.addRows(CatName.length);

    for (var i = 0; i < CatName.length; i++) {
        data.setCell(i, 0, CatName[i]);
        data.setCell(i, 1, CatPoints[i]);
    }

    // Set chart options
    var options = {
        'title': title,
        'width': width,
        'height': height
    };

  

    // Instantiate and draw our chart, passing in some options.
    var chart_div = document.getElementById('chart_div');
    var chart = new google.visualization.PieChart(chart_div);

    google.visualization.events.addListener(chart, 'ready', function () {

        drawCanvas(chart.getImageURI());
       


    });

    chart.draw(data, options);

}


/* 
*Method to draw Column Chart
*/
drawColumnChart = function (newData,title,width,height) {
    var dataTable = new google.visualization.DataTable();


    // determine the number of rows and columns.
    var numRows = ($("#jqGrid").jqGrid('getGridParam', 'colNames')).length;
    var numCols = newData.length;

    var bottomTitle = newData[0].Catagories;
    // in this case the first column is of type 'string'.
 

    dataTable.addColumn('string', newData[0].Catagories);
    // all other columns are of type 'number'.
    for (var i = 1; i < numCols; i++)
        dataTable.addColumn('number', newData[i].Catagories);

    // now add the rows.
    dataTable.addRows(numRows);
    for (var i = 0; i < numRows; i++) {

        //adding rows in google datatable
        var currentRow = newData[i];
        var index = 0;
        for (var key in currentRow) {
            if (key !== undefined && key !== "Catagories") {
                dataTable.setCell(index, i, currentRow[key]);
                index++;
            }

        }
    }
    var options = { // chart options
        title: title,
        width: width,
        height: height,
        bar: { groupWidth: "95%" },
        legend: { position: "right" },
        hAxis: { title: bottomTitle, titleTextStyle: { color: 'black' } }
    };
    var chart_div = document.getElementById('chart_div');
    var chart = new google.visualization.ColumnChart(chart_div);

    google.visualization.events.addListener(chart, 'ready', function () {

        drawCanvas(chart.getImageURI());
        
});

    chart.draw(dataTable, options);

}


/* 
*Method to draw Scatter Chart
*/

drawScatterChart = function (CatName, CatPoints, title, width, height) {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Catagories');
    data.addColumn('number', 'Percent');
    var colNames = jQuery("#jqGrid").jqGrid('getGridParam', 'colNames');
    var yAxisText = colNames[1];
    var xAxisText = colNames[0];
    data.addRows(CatName.length);

    for (var i = 0; i < CatName.length; i++) {
        data.setCell(i, 0, CatPoints[i]);
        data.setCell(i, 1, parseInt(CatName[i]));
    }

    // Set chart options
    var options = {
        title:title,
        hAxis: { title: xAxisText },//, minValue: 0, maxValue: 15 },
        vAxis: { title: yAxisText },//, minValue: 0, maxValue: 15 },
        legend: 'none',
        width: width + 50,
        height:height
    };



    // Instantiate and draw our chart, passing in some options.
    var chart_div = document.getElementById('chart_div');
    var chart = new google.visualization.ScatterChart(chart_div);
    google.visualization.events.addListener(chart, 'ready', function () {
    drawCanvas(chart.getImageURI());
    });

    chart.draw(data, options);

}

/* 
*Method to draw trendlines Chart
*/

drawTrendlines = function (CatName, CatPoints, title, width, height) {
    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Catagories');
    data.addColumn('number', 'Percent');
    var colNames = jQuery("#jqGrid").jqGrid('getGridParam', 'colNames');
    var yAxisText = colNames[0];
    var xAxisText = colNames[1];
    data.addRows(CatName.length);

    for (var i = 0; i < CatName.length; i++) {
        data.setCell(i, 0, CatPoints[i]);
        data.setCell(i, 1,parseInt(CatName[i]));
    }


    var options = {
        title: title,
        hAxis: { title: xAxisText },//, minValue: 0, maxValue: 15 },
        vAxis: { title: yAxisText },//, minValue: 0, maxValue: 15 },
        legend: 'none',
        width: width + 50,
        height:height,
        trendlines: { 0: {} }    // Draw a trendline for data series 0.
    };

    var chart_div = document.getElementById('chart_div');
    var chart = new google.visualization.ScatterChart(chart_div);


    google.visualization.events.addListener(chart, 'ready', function () {

        drawCanvas(chart.getImageURI());
        // console.log(chart_div.innerHTML);


    });

    chart.draw(data, options);


}


/* 
*Method to draw the current chart on HTML5 canvas in order to convert it to JPEG
*/
drawCanvas = function (dataURI) {
    $(".download-buttons").css({ 'visibility': 'visible' });
    

   // imageURI = dataURI;

    var img = new Image();
    img.src = dataURI;
    img.onload = function () { // draw when image is succesfully loaded
        var canvas = document.getElementById("canvas");
        var context = canvas.getContext("2d");

        context.clearRect(0, 0, canvas.width, canvas.height);
        canvas.height = img.height;

        canvas.width = img.width;
        context.drawImage(img, 0, 0);
        
};
   
  
   




    var date = new Date().toISOString().slice(0, 19).replace(/-/g, ""); // set up new unique name for the file to download including date and time
    $('#anc-save-image').attr('href', dataURI).attr("download", "image-" + date + ".png");

    $("#btn-dwnld-pdf").unbind('click');
    $("#btn-dwnld-pdf").click(function () {


        var date = new Date().toISOString().slice(0, 19).replace(/-/g, ""); // set up new unique name for the file to download including date and time
        var imgData = canvas.toDataURL("image/jpeg");
        var doc = new jsPDF();

        doc.addImage(imgData, 'JPEG', 10, 10, (parseInt(img.width)) / 3, (parseInt(img.height)) / 3);
        doc.output('save', "file-" + date + ".pdf"); // download file as PDF

    });


}