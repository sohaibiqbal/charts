﻿
/*Default Data Array For Column Chart */
var columnData = [{ Catagories: "Year", Value1: "2000", Value2: "2001", Value3: "2002", Value4: "2004" }, { Catagories: "Sales", Value1: "10000", Value2: "20000", Value3: "30000", Value4: "40000" }, { Catagories: "Expenses", Value1: "15000", Value2: "20000", Value3: "40000", Value4: "50000" }, { Catagories: "Other", Value1: "35000", Value2: "35000", Value3: "35000", Value4: "35000" }];

/*Default Data Array For Pie Chart */
var pieData = [{ Catagories: "Year", Value1: "2000" }, { Catagories: "Sales", Value1: "10000" }, { Catagories: "Expenses", Value1: "15000" }, { Catagories: "Other", Value1: "35000" }];

/*Default Data Array For Scatter Chart */
var scatterData = [{ Catagories: 1, Value1: 5 }, { Catagories: 2, Value1: 10 }, { Catagories: 3, Value1: 15 }, { Catagories: 4, Value1: 20 }];

/*Default Data Array For Trendlines Chart */
var trendData = [{ Catagories: 1, Value1: 5 }, { Catagories: 2, Value1: 10 }, { Catagories: 3, Value1: 15 }, { Catagories: 4, Value1: 20 }];

/*Global Variables */
var mData = null;
var defaultChartWidth = 500;
var defaultChartHeight = 300;

/*
Method to check whether browser suppor HTML5 Local Storage
*/
checkBrowserSupport = function () {
    if (Modernizr.localstorage)
        return true;
    return false;
}




/*
Method to intilize default or stored data for gridview
*/
loadData =function(chartName) {


    if (typeof (Storage) === "undefined") //check if browser support local storage
        alert("Your browser does not support Local Storage feature ");
    else {

        var data = localStorage.getItem('gridData');// get data from local storage
        if (data === null || JSON.parse(data)[0] !== chartName) {
            //load default data for respective chart
            switch (chartName) {
                case 'pie-chart':
                    mData = pieData;
                    break;
                case 'column-chart':
                    mData = columnData;
                    break;
                case 'scatter-chart':
                    mData = scatterData;
                    break;
                case 'trendlines':
                    mData = trendData;
                    break;
                default:
                    break;
            }
        }

        else {
            mData = JSON.parse(data)[1]; //load the grid data recieved from local storage
        }
    }

    return mData; // return Data Array
}

/*
Method to validate user input in grid cells 
*/
validateString= function(value, column) {
    var charpos = value.search("[^ÂâÀàÆæÇçÉéÊêÈèËëÎîÏïÑñÔôÛûÙùA-Za-z-_. /()]");

    if (value.length > 0 && charpos >= 0) {

        return [false, "Allowed characters  are - A-Z,a-z, - , _ ,.,/,(,)"];
    }
    else {
        return [true, ""];
    }
}

/*
Method to validate if user's input already exists in the grid
*/
validateUniqueness= function(value, column) {

    var columOneValues = $("#jqGrid").jqGrid("getCol", "Catagories");

    if (value.length <= 0) {

        return [false, "Empty catagory name is not allowed"];
    }
    else if (columOneValues.length > 1) {

        for (var i = 0; i < columOneValues.length; i++)
            if (columOneValues[i] !== undefined && (columOneValues[i].toUpperCase() === value.toUpperCase()))
                return [false, "This catagory already exists"];
    }

    return [true, ""];

}
/*
Method to validate if user is trying to save null values
*/
validateNull= function(value, column) {

    if (value.length <= 0)
        return [false, "Empty field name is not allowed"];
    return [true, ""];

}



/*
Method to set up Grid according to given parameters
*/
setGrid = function (numberOfColumns, chartName) {


    var gridColumns = new Array();//initilizing column Model for grid
    gridColumns.push({          // First column is necessary 
        label: 'Catagories',
        name: 'Catagories',
        //  key: true,
        // width: 80,
        align: "center",
        editable: true,
        key: true,
        editrules: {
            //custom rules
            custom_func: validateUniqueness,
            custom: true,
            required: true
        }
    });

    for (var i = 1; i <= numberOfColumns; i++)// create remaining column as user's input
        gridColumns.push({
            label: 'Value' + i,
            name: 'Value' + i,

            //width: 80,
            align: "center",
            editable: true,
            number: true,
            editrules: {
                //custom rules
                custom_func: validateUniqueness, // validate user input in this column
                custom: true,
                required: true
            }
        });


    /*set up grid default properties   */
 $("#jqGrid").jqGrid({
        data: loadData(chartName),
        editurl: 'clientArray',// we set the changes to be made at client side using predefined word clientArray
        datatype: 'local',
        colModel: gridColumns,
        loadonce: false,
        viewrecords: true,
        width: 535,
        scrollOffset: 0,
        height: 200,
        rowNum: 10,
        pager: "#jqGridPager",
        shrinktofit: false,
        cellsubmit: 'clientArray'


    });


    $('#jqGrid').navGrid('#jqGridPager',
         // setting up the delete button to appear on the grid toolbar
        {
            edit: false, add: false, del: true, search: false, refresh: false, view: false, position: "left", cloneToTop: false
        },
            {}, {}, {
                afterComplete: function (response, postdata) {

                    // localStorage["backupData"] = JSON.stringify(mData);

                    //Check the response 
                    return "";
                }
            });
    $('#jqGrid').inlineNav('#jqGridPager',
                   // setting up remaining button to appear on the grid toolbar
                   {

                       edit: true,
                       add: true,
                       del: false,
                       cancel: true,
                       editParams: {
                           keys: true
                       },
                       addParams: {
                           keys: true,
                           position: "last",
                           rowID: function () {  // while adding a new row, providing that row with unique ID 

                               if (mData.length > 0) {
                                   return parseInt(mData[mData.length - 1].id) + 1;
                               } else {
                                   return 1;
                               }
                           }
                       }


                   });


    $("#jqGrid").bind("jqGridInlineAfterSaveRow", function (e, rowid, orgClickEvent) {

        //if (checkBrowserSupport()) {


        //    localStorage.removeItem("gridData");
        //    var dataArray = new Array();
        //    dataArray.push($("#selected-chart").val());
        //    dataArray.push(mData);

        //    localStorage.setItem("gridData", JSON.stringify(dataArray));

        //    // $("#jqGrid").jqGrid('resetSelection');

        //}


        //  if (console) console.debug("inside jqGridInlineAfterSaveRow inline-function");

        return e.result === undefined ? true : e.result;
    });
    /*
     Binding event to Generate Chart Button click
     */
    $("#generateChart").click(function () {

        var chartTitle = $("#chart-title").val();
        var gridData = $("#jqGrid").jqGrid('getRowData');
      
        var numberOfColumns = ($("#jqGrid").jqGrid('getGridParam', 'colNames')).length;
        switch ($("#selected-chart").val()) {
            case 'pie-chart':
                var columOneValues = ($("#jqGrid").jqGrid("getCol", "Catagories"));
                var columTwoValues = ($("#jqGrid").jqGrid("getCol", "Value1"));
                drawPieChart(columOneValues, columTwoValues, chartTitle, defaultChartWidth, defaultChartHeight)
                break;
            case 'column-chart':
                drawColumnChart(gridData, chartTitle, defaultChartWidth, defaultChartHeight);
                break;
            case 'scatter-chart':
                var columOneValues = ($("#jqGrid").jqGrid("getCol", "Catagories"));
                var columTwoValues = ($("#jqGrid").jqGrid("getCol", "Value1"));
                drawScatterChart(columTwoValues, columOneValues, chartTitle, defaultChartWidth, defaultChartHeight)
                break;
            case 'trendlines':
                var columOneValues = ($("#jqGrid").jqGrid("getCol", "Catagories"));
                var columTwoValues = ($("#jqGrid").jqGrid("getCol", "Value1"));
                drawTrendlines(columTwoValues, columOneValues, chartTitle, defaultChartWidth, defaultChartHeight)
                break;
            default:
                break;
        }
    });
    /*
    setting up data to Local storage before page refresh
    */
    $(window).bind('beforeunload', function () {

      
        var numberOfColumns = ($("#jqGrid").jqGrid('getGridParam', 'colNames')).length;
        localStorage.removeItem("gridData");
        var dataArray = new Array();
        dataArray.push($("#selected-chart").val());

        dataArray.push($("#jqGrid").jqGrid('getRowData'));
        dataArray.push(numberOfColumns);

        localStorage.setItem("gridData", JSON.stringify(dataArray));
        setStorageTimeout();

    });



}
/*
set local storage timeout to 30 minutes
*/
setStorageTimeout = function () {

    var hours = 1 / 2; // Reset when storage is more than 24hours
    var now = new Date().getTime();
    var setupTime = localStorage.getItem('setupTime');
    if (setupTime == null) {
        localStorage.setItem('setupTime', now)
    } else {
        if (now - setupTime > hours * 60 * 60 * 1000) {
            localStorage.clear();
            localStorage.setItem('setupTime', now);
        }
    }

}